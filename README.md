# Fundraiseup Test App
## Installation
```bash
npm run init
```
## Setup server
```bash
touch ./server/.env
echo "PORT=*server_port_number*">>./server/.env
echo "DSN=*mongo_dsn*">>./server/.env
```
## Setup client
```bash
touch ./client/.env
echo "VITE_API_BASE_URL=http://localhost:*server_port_number*">>./client/.env
```
## Start
```bash
npm run dev:server
npm run dev:client
```
