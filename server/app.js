const Koa = require('koa');
const Router = require('@koa/router');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const {createDonation} = require('./controllers/donateController');

const app = new Koa();
const router = new Router();

app.use(bodyParser());
app.use(cors());
app.use(async (ctx, next) => {
    ctx.set('Content-Type', 'application/json');
    ctx.set('Accept', 'application/json');
    await next();
});
router.post('/donate', createDonation);
app
    .use(router.routes())
    .use(router.allowedMethods());

module.exports = app;
