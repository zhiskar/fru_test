require('dotenv').config();

module.exports.dsn = process.env.DSN;
module.exports.port = process.env.PORT || 3090;
