const {Donation} = require('../models/Donation');

const CURRENCIES = ['USD', 'EUR', 'GBP', 'RUB'];

/**
 * @param {Object} ctx
 * @param {Function} next
 * @returns {Promise<void>}
 */
const createDonation = async (ctx, next) => {
    if (ctx.is('application/json') === false) {
        ctx.throw(406, 'Server does not accept this data format');
    }

    /**
     * @property {String} currency
     * @property {Number} amount
     */
    const body = ctx.request.body;

    if (
        Object.prototype.hasOwnProperty.call(body, 'amount') === false &&
        Object.prototype.hasOwnProperty.call(body, 'currency') === false
    ) {
        ctx.throw(400, 'Required fields "amount" and "currency" are missing');
    }

    if (Object.prototype.hasOwnProperty.call(body, 'amount') === false) {
        ctx.throw(400, 'Required field "amount" are missing');
    }

    if (Object.prototype.hasOwnProperty.call(body, 'currency') === false) {
        ctx.throw(400, 'Required field "currency" are missing');
    }

    if (
        typeof body.currency !== 'string' ||
        typeof body.amount !== 'number' ||
        body.currency.length === 0 ||
        body.amount <= 0 ||
        !CURRENCIES.includes(body.currency)
    ) {
        ctx.throw(422, 'Sent data has incorrect values');
    }

    await new Donation({
        currency: body.currency,
        amount: body.amount,
    })
        .save()
        .then((savedDonation) => {
            ctx.body = {ok: true};
        })
        .catch((err) => {
            console.error('Error while saving donation in controllers->donateController->createDonation', err.message);
            ctx.throw(500, 'Error while saving donation');
        });

    await next();
};

module.exports.createDonation = createDonation;
