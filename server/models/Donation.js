const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const donationSchema = new Schema(
    {
        currency: {
            type: Schema.Types.String,
            required: true,
        },
        amount: {
            type: Schema.Types.Number,
            required: true,
            min: 0,
        },
    },
    {
        timestamps: true,
    },
);

module.exports.Donation = mongoose.model('Donation', donationSchema, 'donations');
