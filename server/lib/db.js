const mongoose = require('mongoose');
const {dsn} = require('../config')

/**
 * @return {Object}
 */
const connectDb = () => {
    mongoose.connect(
        dsn,
        {
            useUnifiedTopology: true,
            useNewUrlParser: true,
        },
        (err) => {
            if (err) {
                console.error('Mongoose server starting error', err);
            }
        },
    );

    return mongoose.connection;
};

module.exports.connectDb = connectDb;
