const app = require('./app');
const {connectDb} = require('./lib/db')
const {port} = require('./config');

const startServer = () => {
    app.listen(port, async () => {
        console.log(`Server listening on port ${port}`);
    });
};

connectDb()
    .on('error', console.error)
    .on('disconnected', connectDb)
    .on('open', startServer);
