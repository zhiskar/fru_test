type Currency = {
    name: string,
    code: string,
    symbol: string,
    rate: number,
}
