import axios, {AxiosInstance} from 'axios';

const apiClient: AxiosInstance = axios.create({
    // @ts-ignore
    baseURL: import.meta.env.VITE_API_BASE_URL || 'http://localhost:3090',
    headers: {
        'Content-type': 'application/json',
    },
});

export default apiClient;
